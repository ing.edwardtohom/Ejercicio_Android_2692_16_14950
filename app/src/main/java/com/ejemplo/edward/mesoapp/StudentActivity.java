package com.ejemplo.edward.mesoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Edward on 26/07/2017.
 */

public class StudentActivity extends AppCompatActivity {

    private EditText edt1;
    private Button bt1;
    private TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        edt1 = (EditText) findViewById(R.id.edt1);
        bt1=(Button) findViewById(R.id.ok);
        tv1=(TextView) findViewById(R.id.tv1);
    }

    public void agregar(View view){//metodo para sumar numeros

        tv1.setText(String.valueOf("Su Informacion se guardado con exito"));
        edt1.setText("");
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

}
